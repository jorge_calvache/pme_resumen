WITH operacion as(    
      select 
              'EC'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Valentina + Agent' 
                   when max_s = 'ticket-automation' then 'Valentina only'
                   when max_s = 'lupe_app' then 'Agent only' end                    as operation_type

      from EC_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
      group by 1,2
  
union all
      select 
              'CO'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Valentina + Agent' 
                   when max_s = 'ticket-automation' then 'Valentina only'
                   when max_s = 'lupe_app' then 'Agent only' end                    as operation_type

      from CO_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
      group by 1,2
  
union all
      select 
              'PE'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Valentina + Agent' 
                   when max_s = 'ticket-automation' then 'Valentina only'
                   when max_s = 'lupe_app' then 'Agent only' end                    as operation_type

      from PE_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
      group by 1,2
  
union all
      select 
              'AR'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Valentina + Agent' 
                   when max_s = 'ticket-automation' then 'Valentina only'
                   when max_s = 'lupe_app' then 'Agent only' end                    as operation_type

      from AR_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
      group by 1,2
  
union all
      select 
              'BR'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Valentina + Agent' 
                   when max_s = 'ticket-automation' then 'Valentina only'
                   when max_s = 'lupe_app' then 'Agent only' end                    as operation_type

      from BR_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
      group by 1,2

union all
      select 
              'UY'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Valentina + Agent' 
                   when max_s = 'ticket-automation' then 'Valentina only'
                   when max_s = 'lupe_app' then 'Agent only' end                    as operation_type

      from UY_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
      group by 1,2
  
union all
      select 
              'CL'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Valentina + Agent' 
                   when max_s = 'ticket-automation' then 'Valentina only'
                   when max_s = 'lupe_app' then 'Agent only' end                    as operation_type

      from CL_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
      group by 1,2

union all
      select 
              'MX'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Valentina + Agent' 
                   when max_s = 'ticket-automation' then 'Valentina only'
                   when max_s = 'lupe_app' then 'Agent only' end                    as operation_type

      from MX_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
      group by 1,2
  
union all
      select 
              'CR'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Valentina + Agent' 
                   when max_s = 'ticket-automation' then 'Valentina only'
                   when max_s = 'lupe_app' then 'Agent only' end                    as operation_type

      from CR_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
      group by 1,2
),
      
          node_info as
              (
                  select LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
                         LOGS_COMPENSATED_DETAIL.COMPENSATION_DATA,
                         LOGS_COMPENSATED_DETAIL.PRODUCT,
                         LOGS_COMPENSATED_DETAIL.CREATED_AT,
                         coalesce(nodes.key, get(processed_toppings, 0):output_node: key) key_,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1%' then 'it1'
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2%' then 'it2'
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3%' then 'it3'
                                                else 'none'
                                               end) end) end) as                          iteration,
                         split_part(key_, '_', 1)             as                          vertical,
                         split_part(key_, '_', 2)             as                          ticket_type,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3_d_04_06%'
                                                    then replace(KEY_, 'it3_d_04_06', '')
                                                else replace(KEY_, ITERATION, '')
                                               end) end) end) as                          new_key,
                         'AR'                                                             COUNTRY,
                         MAX(try_to_numeric(
                                 coalesce(nodes.rappi_comp, get(processed_toppings, 0):output_node:rappi_comp), 2,
                                 1))                                                      rappi_comp_,
                         count(1)
                  from AR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
                           left join AR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes
                                     on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                         and nodes.type = 'OUTPUT'
                  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

                  UNION ALL

                  select LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
                         LOGS_COMPENSATED_DETAIL.COMPENSATION_DATA,
                         LOGS_COMPENSATED_DETAIL.PRODUCT,
                         LOGS_COMPENSATED_DETAIL.CREATED_AT,
                         coalesce(nodes.key, get(processed_toppings, 0):output_node: key) key_,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1%' then 'it1'
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2%' then 'it2'
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3%' then 'it3'
                                                else 'none'
                                               end) end) end) as                          iteration,
                         split_part(key_, '_', 1)             as                          vertical,
                         split_part(key_, '_', 2)             as                          ticket_type,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3_d_04_06%'
                                                    then replace(KEY_, 'it3_d_04_06', '')
                                                else replace(KEY_, ITERATION, '')
                                               end) end) end) as                          new_key,
                         'PE'                                                             COUNTRY,
                         MAX(try_to_numeric(
                                 coalesce(nodes.rappi_comp, get(processed_toppings, 0):output_node:rappi_comp), 2,
                                 1))                                                      rappi_comp_,
                         count(1)
                  from PE_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
                           left join PE_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes
                                     on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                         and nodes.type = 'OUTPUT'
                  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

                  UNION ALL

                  select LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
                         LOGS_COMPENSATED_DETAIL.COMPENSATION_DATA,
                         LOGS_COMPENSATED_DETAIL.PRODUCT,
                         LOGS_COMPENSATED_DETAIL.CREATED_AT,
                         coalesce(nodes.key, get(processed_toppings, 0):output_node: key) key_,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1%' then 'it1'
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2%' then 'it2'
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3%' then 'it3'
                                                else 'none'
                                               end) end) end) as                          iteration,
                         split_part(key_, '_', 1)             as                          vertical,
                         split_part(key_, '_', 2)             as                          ticket_type,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3_d_04_06%'
                                                    then replace(KEY_, 'it3_d_04_06', '')
                                                else replace(KEY_, ITERATION, '')
                                               end) end) end) as                          new_key,
                         'UY'                                                             COUNTRY,
                         MAX(try_to_numeric(
                                 coalesce(nodes.rappi_comp, get(processed_toppings, 0):output_node:rappi_comp), 2,
                                 1))                                                      rappi_comp_,
                         count(1)
                  from UY_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
                           left join UY_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes
                                     on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                         and nodes.type = 'OUTPUT'
                  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

                  UNION ALL

                  select LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
                         LOGS_COMPENSATED_DETAIL.COMPENSATION_DATA,
                         LOGS_COMPENSATED_DETAIL.PRODUCT,
                         LOGS_COMPENSATED_DETAIL.CREATED_AT,
                         coalesce(nodes.key, get(processed_toppings, 0):output_node: key) key_,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1%' then 'it1'
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2%' then 'it2'
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3%' then 'it3'
                                                else 'none'
                                               end) end) end) as                          iteration,
                         split_part(key_, '_', 1)             as                          vertical,
                         split_part(key_, '_', 2)             as                          ticket_type,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3_d_04_06%'
                                                    then replace(KEY_, 'it3_d_04_06', '')
                                                else replace(KEY_, ITERATION, '')
                                               end) end) end) as                          new_key,
                         'CR'                                                             COUNTRY,
                         MAX(try_to_numeric(
                                 coalesce(nodes.rappi_comp, get(processed_toppings, 0):output_node:rappi_comp), 2,
                                 1))                                                      rappi_comp_,
                         count(1)
                  from CR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
                           left join CR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes
                                     on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                         and nodes.type = 'OUTPUT'
                  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

                  UNION ALL

                  select LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
                         LOGS_COMPENSATED_DETAIL.COMPENSATION_DATA,
                         LOGS_COMPENSATED_DETAIL.PRODUCT,
                         LOGS_COMPENSATED_DETAIL.CREATED_AT,
                         coalesce(nodes.key, get(processed_toppings, 0):output_node: key) key_,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1%' then 'it1'
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2%' then 'it2'
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3%' then 'it3'
                                                else 'none'
                                               end) end) end) as                          iteration,
                         split_part(key_, '_', 1)             as                          vertical,
                         split_part(key_, '_', 2)             as                          ticket_type,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3_d_04_06%'
                                                    then replace(KEY_, 'it3_d_04_06', '')
                                                else replace(KEY_, ITERATION, '')
                                               end) end) end) as                          new_key,
                         'EC'                                                             COUNTRY,
                         MAX(try_to_numeric(
                                 coalesce(nodes.rappi_comp, get(processed_toppings, 0):output_node:rappi_comp), 2,
                                 1))                                                      rappi_comp_,
                         count(1)
                  from EC_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
                           left join EC_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes
                                     on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                         and nodes.type = 'OUTPUT'
                  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

                  UNION ALL

                  select LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
                         LOGS_COMPENSATED_DETAIL.COMPENSATION_DATA,
                         LOGS_COMPENSATED_DETAIL.PRODUCT,
                         LOGS_COMPENSATED_DETAIL.CREATED_AT,
                         coalesce(nodes.key, get(processed_toppings, 0):output_node: key) key_,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1%' then 'it1'
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2%' then 'it2'
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3%' then 'it3'
                                                else 'none'
                                               end) end) end) as                          iteration,
                         split_part(key_, '_', 1)             as                          vertical,
                         split_part(key_, '_', 2)             as                          ticket_type,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3_d_04_06%'
                                                    then replace(KEY_, 'it3_d_04_06', '')
                                                else replace(KEY_, ITERATION, '')
                                               end) end) end) as                          new_key,
                         'CO'                                                             COUNTRY,
                         MAX(try_to_numeric(
                                 coalesce(nodes.rappi_comp, get(processed_toppings, 0):output_node:rappi_comp), 2,
                                 1))                                                      rappi_comp_,
                         count(1)
                  from CO_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
                           left join CO_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes
                                     on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                         and nodes.type = 'OUTPUT'
                  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

                  UNION ALL

                  select LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
                         LOGS_COMPENSATED_DETAIL.COMPENSATION_DATA,
                         LOGS_COMPENSATED_DETAIL.PRODUCT,
                         LOGS_COMPENSATED_DETAIL.CREATED_AT,
                         coalesce(nodes.key, get(processed_toppings, 0):output_node: key) key_,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1%' then 'it1'
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2%' then 'it2'
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3%' then 'it3'
                                                else 'none'
                                               end) end) end) as                          iteration,
                         split_part(key_, '_', 1)             as                          vertical,
                         split_part(key_, '_', 2)             as                          ticket_type,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3_d_04_06%'
                                                    then replace(KEY_, 'it3_d_04_06', '')
                                                else replace(KEY_, ITERATION, '')
                                               end) end) end) as                          new_key,
                         'BR'                                                             COUNTRY,
                         MAX(try_to_numeric(
                                 coalesce(nodes.rappi_comp, get(processed_toppings, 0):output_node:rappi_comp), 2,
                                 1))                                                      rappi_comp_,
                         count(1)
                  from BR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
                           left join BR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes
                                     on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                         and nodes.type = 'OUTPUT'
                  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

                  UNION ALL

                  select LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
                         LOGS_COMPENSATED_DETAIL.COMPENSATION_DATA,
                         LOGS_COMPENSATED_DETAIL.PRODUCT,
                         LOGS_COMPENSATED_DETAIL.CREATED_AT,
                         coalesce(nodes.key, get(processed_toppings, 0):output_node: key) key_,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1%' then 'it1'
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2%' then 'it2'
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3%' then 'it3'
                                                else 'none'
                                               end) end) end) as                          iteration,
                         split_part(key_, '_', 1)             as                          vertical,
                         split_part(key_, '_', 2)             as                          ticket_type,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3_d_04_06%'
                                                    then replace(KEY_, 'it3_d_04_06', '')
                                                else replace(KEY_, ITERATION, '')
                                               end) end) end) as                          new_key,
                         'MX'                                                             COUNTRY,
                         MAX(try_to_numeric(
                                 coalesce(nodes.rappi_comp, get(processed_toppings, 0):output_node:rappi_comp), 2,
                                 1))                                                      rappi_comp_,
                         count(1)
                  from MX_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
                           left join MX_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes
                                     on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                         and nodes.type = 'OUTPUT'
                  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

                  UNION ALL

                  select LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
                         LOGS_COMPENSATED_DETAIL.COMPENSATION_DATA,
                         LOGS_COMPENSATED_DETAIL.PRODUCT,
                         LOGS_COMPENSATED_DETAIL.CREATED_AT,
                         coalesce(nodes.key, get(processed_toppings, 0):output_node: key) key_,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1%' then 'it1'
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2%' then 'it2'
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3%' then 'it3'
                                                else 'none'
                                               end) end) end) as                          iteration,
                         split_part(key_, '_', 1)             as                          vertical,
                         split_part(key_, '_', 2)             as                          ticket_type,
                         (CASE
                              WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')
                              else
                                  (CASE
                                       WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')
                                       else
                                           (CASE
                                                WHEN KEY_::TEXT LIKE '%it3_d_04_06%'
                                                    then replace(KEY_, 'it3_d_04_06', '')
                                                else replace(KEY_, ITERATION, '')
                                               end) end) end) as                          new_key,
                         'CL'                                                             COUNTRY,
                         MAX(try_to_numeric(
                                 coalesce(nodes.rappi_comp, get(processed_toppings, 0):output_node:rappi_comp), 2,
                                 1))                                                      rappi_comp_,
                         count(1)
                  from CL_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
                           left join CL_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes
                                     on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                         and nodes.type = 'OUTPUT'
                  group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
              ),
          node_info_final as (
              select
//        logs_compensated_data.*,
//        node_info.*,
//        logs.ticket_id,
//        logs.order_id
                  distinct logs.ticket_id,
                  iteration,
                  node_info.country,
                  node_info.vertical,
                  node_info.ticket_type,
                  node_info.key_,
                  node_info.created_at,
                  node_info.PRODUCT,
                  ROW_NUMBER() OVER (PARTITION BY COUNTRY, ticket_id ORDER BY node_info.created_at ) AS RW,
                  node_info.rappi_comp_,
                  node_info.new_key
              from AR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                       left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                              node_info.COUNTRY = 'AR'
                       left join AR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs on logs_compensated_data.log_id = logs.id
              where log_id is not null
                and logs.ticket_id is not null


              UNION all

              select
//        logs_compensated_data.*,
//        node_info.*,
//        logs.ticket_id,
//        logs.order_id
                  distinct logs.ticket_id,
                  iteration,
                  node_info.country,
                  node_info.vertical,
                  node_info.ticket_type,
                  node_info.key_,
                  node_info.created_at,
                  node_info.PRODUCT,
                  ROW_NUMBER() OVER (PARTITION BY COUNTRY, ticket_id ORDER BY node_info.created_at) AS RW,
                  node_info.rappi_comp_,
                  node_info.new_key
              from PE_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                       left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                              node_info.COUNTRY = 'PE'
                       left join PE_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs on logs_compensated_data.log_id = logs.id
              where log_id is not null
                and logs.ticket_id is not null

              UNION all

              select
//        logs_compensated_data.*,
//        node_info.*,
//        logs.ticket_id,
//        logs.order_id
                  distinct logs.ticket_id,
                  iteration,
                  node_info.country,
                  node_info.vertical,
                  node_info.ticket_type,
                  node_info.key_,
                  node_info.created_at,
                  node_info.PRODUCT,
                  ROW_NUMBER() OVER (PARTITION BY COUNTRY, ticket_id ORDER BY node_info.created_at) AS RW,
                  node_info.rappi_comp_,
                  node_info.new_key
              from UY_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                       left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                              node_info.COUNTRY = 'UY'
                       left join UY_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs on logs_compensated_data.log_id = logs.id
              where log_id is not null
                and logs.ticket_id is not null

              UNION all

              select
//        logs_compensated_data.*,
//        node_info.*,
//        logs.ticket_id,
//        logs.order_id
                  distinct logs.ticket_id,
                  iteration,
                  node_info.country,
                  node_info.vertical,
                  node_info.ticket_type,
                  node_info.key_,
                  node_info.created_at,
                  node_info.PRODUCT,
                  ROW_NUMBER() OVER (PARTITION BY COUNTRY, ticket_id ORDER BY node_info.created_at) AS RW,
                  node_info.rappi_comp_,
                  node_info.new_key
              from CR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                       left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                              node_info.COUNTRY = 'CR'
                       left join CR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs on logs_compensated_data.log_id = logs.id
              where log_id is not null
                and logs.ticket_id is not null

              UNION all

              select
//        logs_compensated_data.*,
//        node_info.*,
//        logs.ticket_id,
//        logs.order_id
                  distinct logs.ticket_id,
                  iteration,
                  node_info.country,
                  node_info.vertical,
                  node_info.ticket_type,
                  node_info.key_,
                  node_info.created_at,
                  node_info.PRODUCT,
                  ROW_NUMBER() OVER (PARTITION BY COUNTRY, ticket_id ORDER BY node_info.created_at) AS RW,
                  node_info.rappi_comp_,
                  node_info.new_key
              from CO_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                       left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                              node_info.COUNTRY = 'CO'
                       left join CO_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs on logs_compensated_data.log_id = logs.id
              where log_id is not null
                and logs.ticket_id is not null

              UNION all

              select
//        logs_compensated_data.*,
//        node_info.*,
//        logs.ticket_id,
//        logs.order_id
                  distinct logs.ticket_id,
                  iteration,
                  node_info.country,
                  node_info.vertical,
                  node_info.ticket_type,
                  node_info.key_,
                  node_info.created_at,
                  node_info.PRODUCT,
                  ROW_NUMBER() OVER (PARTITION BY COUNTRY, ticket_id ORDER BY node_info.created_at) AS RW,
                  node_info.rappi_comp_,
                  node_info.new_key
              from MX_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                       left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                              node_info.COUNTRY = 'MX'
                       left join MX_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs on logs_compensated_data.log_id = logs.id
              where log_id is not null
                and logs.ticket_id is not null

              UNION all

              select
//        logs_compensated_data.*,
//        node_info.*,
//        logs.ticket_id,
//        logs.order_id
                  distinct logs.ticket_id,
                  iteration,
                  node_info.country,
                  node_info.vertical,
                  node_info.ticket_type,
                  node_info.key_,
                  node_info.created_at,
                  node_info.PRODUCT,
                  ROW_NUMBER() OVER (PARTITION BY COUNTRY, ticket_id ORDER BY node_info.created_at) AS RW,
                  node_info.rappi_comp_,
                  node_info.new_key
              from EC_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                       left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                              node_info.COUNTRY = 'EC'
                       left join EC_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs on logs_compensated_data.log_id = logs.id
              where log_id is not null
                and logs.ticket_id is not null

              UNION all

              select
//        logs_compensated_data.*,
//        node_info.*,
//        logs.ticket_id,
//        logs.order_id
                  distinct logs.ticket_id,
                  iteration,
                  node_info.country,
                  node_info.vertical,
                  node_info.ticket_type,
                  node_info.key_,
                  node_info.created_at,
                  node_info.PRODUCT,
                  ROW_NUMBER() OVER (PARTITION BY COUNTRY, ticket_id ORDER BY node_info.created_at) AS RW,
                  node_info.rappi_comp_,
                  node_info.new_key
              from BR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                       left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                              node_info.COUNTRY = 'BR'
                       left join BR_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs on logs_compensated_data.log_id = logs.id
              where log_id is not null
                and logs.ticket_id is not null

              UNION all

              select
//        logs_compensated_data.*,
//        node_info.*,
//        logs.ticket_id,
//        logs.order_id
                  distinct logs.ticket_id,
                  iteration,
                  node_info.country,
                  node_info.vertical,
                  node_info.ticket_type,
                  node_info.key_,
                  node_info.created_at,
                  node_info.PRODUCT,
                  ROW_NUMBER() OVER (PARTITION BY COUNTRY, ticket_id ORDER BY node_info.created_at) AS RW,
                  node_info.rappi_comp_,
                  node_info.new_key
              from CL_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                       left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                              node_info.COUNTRY = 'CL'
                       left join CL_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs on logs_compensated_data.log_id = logs.id
              where log_id is not null
                and logs.ticket_id is not null
          ),
          one_ticket as (
              select *
              from node_info_final
              where rw = 1
          ),
              scope as (
        select distinct
            ticket_id as ticket_id_scope
        from ops_global.cs_inflow
        where inflow_start >= date_trunc('week', dateadd('month', -5, current_date))
    ),
        automation_type as
	(
		select distinct
			c.kustomer_conversation_id::text as ticket_id,
			'Kustomer' as center,
			case when c.tags like '%5f5f9719a242830012dac0a7%' then 'whatsapp automation' else 'decision manager' end as automation_type,
		    true as automation,
		    c.status

		from scope st
		inner join fivetran.co_pg_ms_kustomer_etl_public.conversations c on st.ticket_id_scope::text = c.kustomer_conversation_id::text
				and (
						c.custom:ticketAutomationSolutionTypeStr::text like 'auto%'
						or c.custom:ticketAutomationExitRuleStr::text like 'auto%'
						or c.tags like '%5f5f9719a242830012dac0a7%'

					)
		qualify row_number () over (partition by c.kustomer_conversation_id order by c.kustomer_created_at) = 1
	),

          inflow_count as
              (select country,
                      ticket_id,
                      sum(case when inflow_event != 'agent_reassingment' then 1 else 0 end)       as total_inflow,
                      sum(case when inflow_event like '%reopen%' then 1 else 0 end)               as reopen_count,
                      sum(case when inflow_event = 'non_live_queue_escalation' then 1 else 0 end) as queue_escalations,
                      sum(case
                              when inflow_event in ('non_live_queue_escalation', 'agent_reassingment') then 1
                              else 0 end)                                                         as quantity_transf,
                      sum(case when automation then 1 else 0 end)                                 as automated_resolutions,
                      sum(case when automation_type = 'decision manager' then 1 else 0 end)       as aut_res_type_decision_manager,
                      sum(case when automation_type = 'live automation' then 1 else 0 end)        as aut_res_type_live_automation,
                      sum(case when automation_type = 'order canceled' then 1 else 0 end)         as aut_res_type_order_canceled,
                      sum(case when automation_type = 'order closed' then 1 else 0 end)           as aut_res_type_order_closed,
                      sum(case when automation_type = 'whatsapp automation' then 1 else 0 end)    as aut_res_type_whatsapp_automation,
                      sum(case when automation_type = 'workflow automation' then 1 else 0 end)    as aut_res_type_workflow_automation

               from ops_global.cs_inflow
               group by 1, 2
              ),

         inflow_workaround as (
        select
            inflow.center,
            country,
            inflow.ticket_id,
            inflow_event,
            inflow_start,
            user_type,
            order_id,
            case when inflow_start >= '2021-08-23' and inflow_event = 'non_live_open'
                and inflow.automation_type = 'open ticket' and status = 'done' then coalesce(automation_type.automation_type, 'not automated')
                else inflow.automation_type end as automation_type,
            case when inflow_start >= '2021-08-23' and inflow_event = 'non_live_open'
                and inflow.automation_type = 'open ticket' and status = 'done' then coalesce(automation_type.automation, false)
                else inflow.automation end as automation
        from scope
        left join ops_global.cs_inflow inflow on scope.ticket_id_scope = inflow.ticket_id
        left join automation_type on scope.ticket_id_scope = automation_type.ticket_id
            and inflow_event not in ('agent_reassingment')
    ),

    inflow_pre as (
        select
            center,
            country,
            ticket_id,
            inflow_event,
            inflow_start,
            automation,
            user_type,
            order_id,
            case
                when inflow_event = 'non_live_reopen' then 1
                when inflow_event = 'live_reopen' then 1
                else 0
            end as has_reopen,
            /* General Inflow */
            1 as has_inflow,
            case when automation_type not in ('not automated','open ticket') then 1 else 0 end as inflow_automated,
            case when automation_type in ('not automated','open ticket') then 1 else 0 end as inflow_not_automated,
            case when automation_type <> 'decision manager' then 1 else 0 end as inflow_not_decision_manager,

            /* Detailed automation */
            case when automation_type = 'workflow automation' then 1 else 0 end as workflow_automation,
            case when automation_type = 'whatsapp automation' then 1 else 0 end as whatsapp_automation,
            case when automation_type = 'order closed' then 1 else 0 end as order_closed_automation,
            case when automation_type = 'order canceled' then 1 else 0 end as order_canceled_automation,
            case when automation_type = 'live automation' then 1 else 0 end as live_automation,
            case when automation_type = 'decision manager' then 1 else 0 end as decision_manager_automation,
            case when automation_type = 'open ticket' then 1 else 0 end as open_ticket
        from inflow_workaround
--         from scope
--         left join ops_global.cs_inflow inflow on scope.ticket_id_scope = inflow.ticket_id
--             and inflow_event not in ('agent_reassingment')
    ),



    inflow as (
        select
            center,
            country,
            ticket_id,

            /* General Inflow */
            sum(has_inflow) as inflow,
            sum(inflow_automated) as inflow_automated,
            sum(inflow_not_automated) as inflow_not_automated,
            sum(inflow_not_decision_manager) as inflow_not_decision_manager,

            /* Detailed automation */
            sum(i.workflow_automation) as workflow_automation,
            sum(i.whatsapp_automation) as whatsapp_automation,
            sum(i.order_closed_automation) as order_closed_automation,
            sum(i.order_canceled_automation) as order_canceled_automation,
            sum(i.live_automation) as live_automation,
            sum(i.decision_manager_automation) as decision_manager_automation,

            /* First iteration classification */
            sum(case when inflow_event = 'non_live_open' and i.decision_manager_automation = 1 then 1 else 0 end) > 0 as dm_automation,
            sum(case when inflow_event = 'non_live_open' and i.whatsapp_automation = 1 then 1 else 0 end) > 0 as wpp_automation,
            sum(case when inflow_event = 'non_live_open' and i.workflow_automation = 1 then 1 else 0 end) > 0 as wf_nl_automation,
            sum(case when inflow_event = 'live_open' and i.order_closed_automation = 1 then 1 else 0 end) > 0 as ocl_automation,
            sum(case when inflow_event = 'live_open' and i.order_canceled_automation = 1 then 1 else 0 end) > 0 as ocn_automation,
            sum(case when inflow_event = 'live_open' and i.live_automation = 1 then 1 else 0 end) > 0 as la_automation,
            sum(case when inflow_event = 'live_open' and i.workflow_automation = 1 then 1 else 0 end) > 0 as wf_l_automation,

            /* Reopen */
            sum(has_reopen) as reopen
        from inflow_pre i
        group by 1, 2, 3
    ),



          PRE_PROBLEMAS AS (
              SELECT CS.COUNTRY,
                     --CS.ORDER_ID,
                     --TICKET_ID,
                     global_orders.VERTICAL_GROUP                                         AS VERTICAL,
                     CS.KUSTOMER_CREATED_AT::date                                         AS TICKET_DATE,
                     CS.USER_TYPE,
                     CS.LEVEL_1                                                           AS CATEGORY_LEVEL_1,
                     CS.LEVEL_2                                                           AS SUB_CATEGORY_LEVEL_1,
                     CS.PRIORITY_A_SEGMENTATION                                           AS USER_VALUE_FINANCE,
                     CS.HVU_VALUE_SEGMENT                                                 AS USER_VALUE_HVU,
                     CS.LEVEL_1_AGENT                                                     AS AGENT_LEVEL_1,
                     ci.automated_resolutions > 0                                         as IS_AUTOMATED,
                     C.automatic_solution_type                                            as SOLUTION_TYPE,
                     ci.reopen_count                                                      as REOPEN,
                     IS_DEFECT,
                     0                                                                       HAS_COMPENSATION,
                     node.iteration,
                     node.ticket_type,
                     node.new_key                                                         as key_,
                     coalesce(C.CUSTOM:ticketAutomationExitRuleStr::text,
                              C.CUSTOM:dMexitruleTree::text)                              as ticketAutomationExitRule,
                     replace(C.CUSTOM['badStateReasonStr'], '"', '')                      as bad_reason,
                     coalesce(replace(C.CUSTOM['financeUserTypeStr'], '"', ''),
                              A.type_user)                                                as finance_user_type,
                     coalesce(cs.segment_rfm,replace(C.CUSTOM['rfmSegmentStr'], '"', '')) as rfm_segment,
                     A.category,
                     'Non-Live'                                                              LIVE_NON_LIVE,
                     CASE WHEN cs.KUSTOMER_QUEUE_ID = '61b29db70b6b33c9a4f919dc' OR coalesce(C.CUSTOM:ticketAutomationExitRuleStr::text,
                              C.CUSTOM:dMexitruleTree::text) ilike '%RC_REFUND_V3_POSIBLE_FRAUD_RISK%' then true else false end as filtro_fraude,
        

                     cs.CHANNEL                                                           as PRE_CHANNEL,
                     case
                         when rfm_segment
                                  in ('None', 'Zero', 'Low Value', 'Regular', 'Undefined Segment RFM','Potencial Power')
                             or cs.KUSTOMER_QUEUE_ID = '60ca0577622232001eae924b'
                             THEN 'Low Users'
                         when o1.is_marketplace = 'TRUE' and o1.payment_method = 'cash' and o1.row1 = 1
                             or cs.KUSTOMER_QUEUE_ID = '60cbab402504950019c82353' THEN 'Market Place'
                         when o1.is_marketplace = 'TRUE'
                             or cs.KUSTOMER_QUEUE_ID = '60cbab402504950019c82353' THEN 'Market Place'
                         when wu.usr_whales = 'whale' then 'User_Whales'
                         else 'Resto' end                                                 as low_users_nss,
                        /*coalesce( MAX(CASE
                             WHEN IS_AUTOMATED = TRUE AND reopen > 0 AND SOLUTION_TYPE != 'manual_review'
                                 THEN 'Valentina + Agent'
                             WHEN IS_AUTOMATED = TRUE THEN 'Valentina only'
                             WHEN IS_AUTOMATED = FALSE THEN 'Agent only'
                             ELSE '' END),max(operacion.operation_type) )                                               AS AUTOMATION_AGENTS,*/
                                   max( case
                when inflow.reopen = 0 and inflow.inflow_not_decision_manager = 0 then 'Valentina only'
                when inflow.decision_manager_automation = 0 then 'Agent only'
                when inflow.reopen > 0 and inflow.decision_manager_automation > 0 then 'Valentina + Agent'
                else 'Agent only'
                end) as automation_agents,

                     0                                                                    AS FULL_ASKED_COMPENSATION,
                     SUM(CASE WHEN SURVEY_RATING > 0 THEN 1 ELSE 0 END)                   AS NO_OF_RATINGS,
                     SUM(ci.reopen_count)                                                 AS REOPEN_COUNT,
                  /*SUM(COMPENSATION_USD)  */0                                            AS COMPENSATION_USD,
                     SUM(SURVEY_RATING)                                                   as SCORE,
                     SUM(CASE
                             WHEN SURVEY_RATING = 5 THEN 1
                             WHEN SURVEY_RATING = 4 THEN 0
                             WHEN SURVEY_RATING <= 3 then -1
                             ELSE NULL END)                                               AS NSS,
                     SUM(TOTAL_INFLOW)                                                    AS TOTAL_INFLOW,
                     COUNT(CS.KUSTOMER_CONVERSATION_ID)                                   AS DEFECTS,
                     MAX(global_orders.VERTICAL_GROUP)                                    AS VERTICAL_SUB_GROUP
              FROM OPS_GLOBAL.NON_LIVE_TICKETS_DETAILS_V2 CS
                       LEFT JOIN FIVETRAN.CO_PG_MS_KUSTOMER_ETL_PUBLIC.CONVERSATIONS C
                                 on upper(case
                                              when substr(lower(trim(C.country)), 1, 2) not in
                                                   ('ar', 'br', 'cl', 'co', 'cr', 'ec', 'mx', 'pe', 'uy') then null
                                              else substr(lower(trim(C.country)), 1, 2) end) = CS.COUNTRY
                                     and c.KUSTOMER_CONVERSATION_ID = CS.KUSTOMER_CONVERSATION_ID
                       --left join pilot on pilot.order_id = CS.order_id and CS.country = pilot.country
                       left join (select country,
                                         application_user_id,
                                         order_id,
                                         created_at,
                                         payment_method,
                                         is_marketplace,
                                         state_type,
                                         row_number()
                                                 over (partition by country, application_user_id order by created_at asc) as row1
                                  from ops_global.global_orders
                                  where state_type = 'finished' qualify row1 = 1) o1
                                 on cs.country = o1.country and cs.order_id = o1.order_id


                       LEFT JOIN (SELECT *,
                                         TIMEADD(SECOND, -1,
                                                 CASE WHEN MC_ACTUAL = 1 THEN CURRENT_DATE + 1 ELSE END_DATE END) END_DATE_
                                  FROM GLOBAL_FINANCES.GLOBAL_APPLICATION_USERS_HIST) A
                                 ON A.COUNTRY = cs.COUNTRY AND A.ID = cs.APPLICATION_USER_ID AND
                                    CS.KUSTOMER_CREATED_AT::TIMESTAMP BETWEEN A.START_DATE::TIMESTAMP AND A.END_DATE_::TIMESTAMP

                       left join one_ticket node
                                 on node.ticket_id = CS.KUSTOMER_CONVERSATION_ID and cs.COUNTRY = node.COUNTRY
                       left join ops_global.users_whales_mx wu
                                 on wu.COUNTRY = cs.COUNTRY and wu.APPLICATION_USER_ID = cs.application_user_id
                       LEFT JOIN GLOBAL_FINANCES.global_orders
                                 ON global_orders.ORDER_ID = CS.ORDER_ID AND global_orders.COUNTRY = CS.COUNTRY
                       left join inflow_count ci
                                 on (cs.KUSTOMER_CONVERSATION_ID = ci.ticket_id and cs.country = ci.country)
                       left join inflow on inflow.ticket_id = cs.kustomer_conversation_id and inflow.country = cs.country
            
                       left join operacion on operacion.order_id = cs.order_id and operacion.country = cs.country
              WHERE CS.KUSTOMER_CREATED_AT >= DATEADD(MONTH, -5, DATE_TRUNC(MONTH, CURRENT_DATE))
              GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,26
            
          )


     SELECT 'DAILY'                            AS CALENDAR_TYPE,
            DATE_TRUNC(DAY, TICKET_DATE)::DATE AS DATES,
            PRE_CHANNEL                        AS CHANNEL,
            PRE_PROBLEMAS.*
     FROM PRE_PROBLEMAS
     WHERE IS_DEFECT
       AND USER_TYPE = 'Customer'
       AND LIVE_NON_LIVE = 'Non-Live'


     UNION ALL

     SELECT 'WEEKLY'                            AS CALENDAR_TYPE,
            DATE_TRUNC(WEEK, TICKET_DATE)::DATE AS DATES,
            PRE_CHANNEL                         AS CHANNEL,
            PRE_PROBLEMAS.*
     FROM PRE_PROBLEMAS
     WHERE IS_DEFECT
       AND USER_TYPE = 'Customer'
       AND LIVE_NON_LIVE = 'Non-Live'

     UNION ALL

     SELECT 'MONTHLY'                            AS CALENDAR_TYPE,
            DATE_TRUNC(MONTH, TICKET_DATE)::DATE AS DATES,
            PRE_CHANNEL                          AS CHANNEL,
            PRE_PROBLEMAS.*
     FROM PRE_PROBLEMAS
     WHERE IS_DEFECT
       AND USER_TYPE = 'Customer'
       AND LIVE_NON_LIVE = 'Non-Live'
